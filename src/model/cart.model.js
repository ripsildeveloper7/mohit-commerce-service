var mongoose = require('mongoose');

const cartSchema = new mongoose.Schema({
    userId: String,
    items: [{productId: {type: mongoose.Schema.Types.ObjectId, ref: 'ProductSchema'  }, sku: String, qty: Number } ]
  });

  /* const Cart = mongoose.model('cart', cartSchema);
  module.exports = Cart; */
