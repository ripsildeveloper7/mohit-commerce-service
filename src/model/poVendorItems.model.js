var mongoose = require('mongoose');

var PoVendorItems = mongoose.Schema({
    catalogName: String,
    vendorItemNo: String,
    unitPrice: Number,
    size: String,
    quantity: Number,
    unitOfMeasure: String,
    discountPercentage: Number,
    cgstRate: Number,
    sgstRate: Number,
    igstRate: Number,
    cgstAmount: Number,
    sgstAmount: Number,
    igstAmount: Number,
    netAmount: Number,
    estimatedShipmentDate: Date
   
});
module.exports = PoVendorItems;