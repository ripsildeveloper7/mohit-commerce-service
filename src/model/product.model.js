var mongoose = require('mongoose');
var size = require('./size.model');

const ProductSchema = new mongoose.Schema({
  productName: String,
  productDescription: String,
 
  styleCode: String,
  quantity: Number,
  productImageName: [String],
  manufacturer: String,
  price: Number,
  bulletPoints: String,
  height: String,
  weight: String,
  material: String,
  occassion: String,
  color: String,
  // category map
  superCategoryId: String,
  mainCategoryId: String,
  subCategoryId: String,

  // meta tag start
  metaTitle: String,
  metaDescription: String,
  metaKeyword: String,
 
  // meta tag end
  dateAdded: Date,
  dateModified: Date,
  brandId: String,
  sizeVariantId: String,
  productVariant: [size],

  // ready to wear
  serviceActive: Boolean,
  serviceName: String,
  serviceType: String,
  serviceAmount: Number,
  serviceDiscount: Number
  
});
/* 
const product = mongoose.model('product', ProductSchema);
module.exports = product; */