var mongoose = require('mongoose');
var poVendorItems = require('./poVendorItems.model');

var PurchaseOrderSchema = new mongoose.Schema({
    poNumber: String,
    poType: String,
    poDate: Date,
    poExpiryDate: Date,
    billTo: String,
    gstNo: String,
    contactNo: String,
    vendorName: String,
    vendorId: String,
    vendorAddress: String,
    vendorEmailId: String,
    vendorCSTNo: String,
    vendorGStNo: String,
    vendorCurrencyType: String,
    netAmount: Number,
    freight: Number,
    tax: Number,
    specialInstruction: String,
    totalAmount: Number,
    vendorItems: [poVendorItems],
    orderId: String
});

var PurchaseOrder = mongoose.model('purchaseorder', PurchaseOrderSchema);
module.exports = PurchaseOrder;