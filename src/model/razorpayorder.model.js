var mongoose = require('mongoose');
var Product = require('./product.model');
var Cart = require('./cart.model');
var Coupon = require('./coupon.model');
const RazorpayOrderSchema = new mongoose.Schema({
    customerId: String,
    orderId: String,
    items: [{
        productId: mongoose.Schema.Types.ObjectId, pack: Number, moq: Number,
        ratioQty: Number
    }],
    total: Number,
    addressDetails: [{
        name: String,
        mobileNumber: Number,
        streetAddress: String,
        building: String,
        landmark: String,
        city: String,
        state: String,
        pincode: String,
        country: String
    }],
    paymentStatus: String,
    orderStatus: String,
    orderDate: Date,
    orderUpdatedDate: Date,
    razorPayOrderId: String,
    paymentStatus: String,
    // product details
    orderedProducts: [
        Product
    ],
    // cart details
    cart: [
        Cart
    ],
    // payment details
    razorpayPaymentId: String,
    razorpaySignature: String,
    paymentStatus: String,
    paymentMode: String,
    shipmentStatus: String,
    paypalOrderId: String,
    purchaseOrderStatus: String,
    // coupon details
    coupon: [Coupon]
});
const Order = mongoose.model('razorpayorder', RazorpayOrderSchema);
module.exports = Order;