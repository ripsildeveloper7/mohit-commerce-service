var mongoose = require('mongoose');
const ShippingAreaSchema = new mongoose.Schema({
    serviceType: String,
pincodes: [String]
});
const ShippingArea = mongoose.model('shippingarea', ShippingAreaSchema);
module.exports = ShippingArea;