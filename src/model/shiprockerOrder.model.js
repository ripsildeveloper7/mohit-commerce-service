var mongoose = require('mongoose');

const shiprocketOrderSchema = new mongoose.Schema({
    order_id: String,
    shipment_id: String,
    status: String,
    status_code: Number,
    onboarding_completed_now: Number,
    websiteOrderId: String,
    orderObjectId: String,
    awbNo: String



});

const ShiprocketOrder = mongoose.model('shiprocketorder', shiprocketOrderSchema);
module.exports = ShiprocketOrder;
