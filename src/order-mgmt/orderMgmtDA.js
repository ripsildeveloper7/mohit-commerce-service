var Orders = require('../model/order.model');
var Razorpay = require('razorpay');
var request = require('request');
/* var instance = new Razorpay({
  key_id: 'rzp_test_RsTPCyMGBjd2Rs',
  key_secret: 'aN208ePr0dyec97BKmwoFyMO'
}) */
var instance = new Razorpay({
  key_id: 'rzp_test_jYsiE0QsL9LkgB',
  key_secret: 'R5nvOnsF9iWuK3R9xpUHozW5'
})


exports.viewOrders = function (req, res) {
  Orders.find({'paymentStatus' : 'Success' }).select().exec(function (err, allOrders) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving all orders."
      });
    } else {

      res.status(200).json(allOrders);
    }
  });
}
exports.viewCustomerOrders = function (req, res) {
  Orders.find({
    'customerId': req.params.id,
    'paymentStatus' : 'Success'
  }).sort({
    orderId: -1
  }).exec(function (err, details) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(details);
    }
  });
}
exports.uniqueOrderView = function (req, res) {
  Orders.findOne({
    '_id': req.params.id
  }).sort({
    orderId: -1
  }).exec(function (err, details) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(details);
    }
  });
}

exports.totalOrdersCount = function (req, res) {
  Orders.find({}).select().exec(function (err, allOrders) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving all orders."
      });
    } else {
      console.log(allOrders.length);
      res.status(200).json(allOrders.length);
    }
  });
}
exports.orderStatusUpdate = function (req, res) {
  Orders.findOne({
    _id: req.params.id
  }).select().exec(function (err, orderData) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving all orders."
      });
    } else {
      orderData.orderStatus = req.body.orderStatus;
      orderData.save(function (err, orders) {
        if (err) {
          console.log(err);
        } else {
          Orders.findOne({
            '_id': req.params.id
          }).sort({
            orderId: -1
          }).exec(function (err, details) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(details);
            }
          });
        }
      })

    }
  });
}

exports.newOrderAdd = function (req, res, orderId) {   
  /* var inSms = '9567944414';  // admin mobile number
  smsHeader = 'LENCOT'; */
 smsBody = 'New Order' + ' ' + orderId + 'Has been Placed';
  var order = new Orders();
  order.purchaseOrderStatus = 'New',
  order.customerId = req.body.customerId;
  order.orderId = orderId;
  order.paymentStatus = 'Success';
  order.addressDetails = req.body.addressDetails;
  order.cart = req.body.cart;
  order.coupon = req.body.coupon;
  order.orderedProducts = req.body.orderedProducts;
  order.paymentMode = req.body.paymentMode; 
  order.paypalOrderId = req.body.paypalOrderId;
  order.orderStatus = 'New';
  /* order.paymentStatus = 'Pending'; */
  order.orderDate = new Date();
  order.total = req.body.total;
  order.save(function (err, data) {
    if (err) {
      res.status(500).json(err)
    } else {
      var options = {
        // amount in pase
        amount: req.body.total * 100,
        currency: "INR",
        receipt: "RCPTID43",
        payment_capture: '1'
      };
      res.status(200).json(data);
      // send msg to admin
     /*  request(
        'http://login.bulksmsgateway.in/sendmessage.php?user=lencott&password=lencott@123&mobile=' +
        inSms+ '&message=' +
       smsBody + '&sender=' +
        smsHeader + '&type=3',
        function (err, smsCheck) {
          if (err) {
            console.log(err);
            res.status(500).send({
              message: "Some error occurred while retrieving notes."
            });
          } else {
            console.log(smsCheck);
            smsStatus = JSON.parse(smsCheck.body).status;
          }
        }
      ); */
    }
  });
}
exports.addRazorPayDetails = function (req, res) {
  Orders.findOne({
    '_id': req.params.id
  }).select().exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      const MOBILENUMBER = data.addressDetails[0].mobileNumber;
      var txtMsg = 'Thank you for placing the order ' + data.orderId + '. We will update you shortly.';
      var adminMsg = 'New Order Has been Placed' + data.orderId;
      var adminNumber = 9902179192;  // admin number
      data.razorpayPaymentId = req.body.paymentId;
      data.razorpaySignature = req.body.razorpaySignature;
      data.paymentStatus = 'Success';
      data.total = req.params.total;
      data.save(function (err, data) {
        if (err) {
          res.status(500).json(err);
        } else { 
          // send sms to customer
          request('https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=SDM6ldRypUCB1uODxc5u8w&senderid=SMSTST&channel=2&DCS=0&flashsms=0&number='+MOBILENUMBER+'&text='+txtMsg, function(err, data) {
            if(err) {
              console.log(err);
            } else {
              // send sms to admin
              request('https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=SDM6ldRypUCB1uODxc5u8w&senderid=SMSTST&channel=2&DCS=0&flashsms=0&number='+adminNumber+'&text='+adminMsg, function(err, data) {
               if(err) {
                 res.status(500).json(err);
               } else {
                 console.log(data);
               }
            })
              console.log(data)
            }
          })
          res.status(200).json(data);
        }
      })
    }
  })
}

exports.razorpayNewOrder = function (req, res, orderId) {
  var order = new Orders();
  order.customerId = req.body.customerId;
  order.purchaseOrderStatus = 'New';
  order.orderId = orderId;
  order.addressDetails = req.body.addressDetails;
  order.cart = req.body.cart;
  order.orderedProducts = req.body.orderedProducts;
  order.paymentMode = req.body.paymentMode;
  order.orderStatus = 'New';
  order.paymentStatus = 'Pending';
  order.orderDate = new Date();
  order.total = req.body.total;
  order.save(function (err, data) {
    if (err) {
      res.status(500).json(err)
    } else {
      var options = {
        // amount in pase
        amount: req.body.total * 100 ,
        currency: "INR",
        receipt: "RCPTID43",
        payment_capture: '1'
      };
      instance.orders.create(options, function (err, order) {
        if (err) {
          res.status(500).json(err)
          console.log(err);
        } else {
          Orders.findOne({
            orderId: data.orderId
          }).select().exec(function (err, orderDetails) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving orders."
              });
            } else {
              orderDetails.razorPayOrderId = order.id;
              orderDetails.save(function (err, savedOrder) {
                if (err) {
                  res.status(500).json(err)
                  console.log(err);
                } else {
                  /* console.log(savedOrder); */
                }
              })
              res.status(200).json(orderDetails);
            }
          });
        }

      });
    }
  });
}

// paypal order creation
exports.paypalOrder = function (req, res) {

    
    var CREATE_PAYMENT_URL = 'https://mystore.com/api/paypal/create-payment';

    return (CREATE_PAYMENT_URL, {
      method: 'post',
      headers: {
        'content-type': 'application/json'
      }
    }).then(function(res) {
      return res.json();
    }).then(function(data) {
      return data.token;
    });
}

exports.updatePOstatus = function(req, res) {
  Orders.findOne({'_id': req.params.id}).select().exec(function(err, order) {
    if (err) {
      res.status(500).json(err);
    } else {
      order.purchaseOrderStatus = req.body.purchaseOrderStatus;
      order.save(function(err, update) {
        if (err) {
          res.status(500).json(err);
        } else {
          res.status(200).json(update);
        }
      })
    }
  })
}
exports.checkCouponOnCustomer = function(req, res) {
  Orders.find({
      'customerId': req.body.customerId,
      'coupon.couponCode': req.body.couponCode
  }).select().exec(function(err, order) {
    if (err) {
      res.status(500).json(err);
    } else {
       res.status(200).json(order);
    }
  })
}
exports.checkCustomerOrderForFirtTimeCoupon = function(req, res) {
  Orders.find({'customerId': req.params.id}).select().exec(function(err, order) {
     if (err) {
       res.status(500).json(err);
     } else {
       res.status(200).json(order);
     }
  })
}

exports.getAllOrderWithDate = function(req,res) {
  Orders.find({paymentStatus:'Success'}).select('orderDate').exec(function(err, order){
    if(err){
      res.status(500).json(err);
    } else {
      res.status(200).json(order);
    }
  })
}