var orderMgmtDA = require('./orderMgmtDA');
var Order = require('../model/order.model');
var zeroFill = require('zero-fill');

exports.viewOrders = function (req, res) {
    try {
        orderMgmtDA.viewOrders(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.viewCustomerOrders = function (req, res) {
    try {
        orderMgmtDA.viewCustomerOrders(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uniqueOrderView = function (req, res) {
    try {
        orderMgmtDA.uniqueOrderView(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.totalOrdersCount = function (req, res) {
    try {
        orderMgmtDA.totalOrdersCount(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.orderStatusUpdate = function (req, res) {
    try {
        orderMgmtDA.orderStatusUpdate(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.orderStatusUpdate = function (req, res) {
    try {
        orderMgmtDA.orderStatusUpdate(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.newOrderAdd = function (req, res) {
    try {
        var currentDate = new Date();
        var day = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        var date = day + "/" + month + "/" + year;


        var oYear = year.toString();
        var orderYear = oYear.slice(-2);
        var order = "ORD";
        var locale = "en-us";
        var result = currentDate.toLocaleString(locale, {
            month: "long"
        });
        var orderMonth = result.substr(0, 3).toUpperCase();

        Order.find().select().exec(function (err, details) {
            if (err) {
                res.status(500).send({
                    message: "Some error occurred while retrieving notes."
                });
            } else {
                if (details[0] == null) {
                    var orderID = order + orderYear + orderMonth + "0001";
                    orderMgmtDA.newOrderAdd(req, res, orderID);
                } else {
                    var arrayLength = details.length - 1;
                    var maxID = details[arrayLength].orderId.substr(10, 3);
                    var incOrder = maxID.slice(-4);
                    var addZero = zeroFill(4, 1);
                    var result = parseInt(incOrder) + parseInt(addZero);
                    var results = zeroFill(4, result);
                    var orderID = order + orderYear + orderMonth + results;
                    orderMgmtDA.newOrderAdd(req, res, orderID);
                }
            }

        })

    } catch (error) {
        console.log(error);
    }
}
exports.addRazorPayDetails = function (req, res) {
    try {
        orderMgmtDA.addRazorPayDetails(req, res);
    } catch (error) {
        console.log(error);
    }
}

/* exports.newOrder = function (req, res) {
    try {
        orderMgmtDA.newOrder(req, res);
    } catch (error) {
        console.log(error);
    }
} */
exports.paypalOrder = function (req, res) {
    try {
        orderMgmtDA.paypalOrder(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.razorpayNewOrder = function (req, res) {
    try {
        var currentDate = new Date();
        var day = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        var date = day + "/" + month + "/" + year;


        var oYear = year.toString();
        var orderYear = oYear.slice(-2);
        var order = "ORD";
        var locale = "en-us";
        var result = currentDate.toLocaleString(locale, {
            month: "long"
        });
        var orderMonth = result.substr(0, 3).toUpperCase();

        Order.find().select().exec(function (err, details) {
            if (err) {
                res.status(500).send({
                    message: "Some error occurred while retrieving notes."
                });
            } else {
                if (details[0] == null) {
                    var orderID = order + orderYear + orderMonth + "0001";
                    orderMgmtDA.razorpayNewOrder(req, res, orderID);
                } else {
                    var arrayLength = details.length - 1;
                    var maxID = details[arrayLength].orderId.substr(10, 3);
                    var incOrder = maxID.slice(-4);
                    var addZero = zeroFill(4, 1);
                    var result = parseInt(incOrder) + parseInt(addZero);
                    var results = zeroFill(4, result);
                    var orderID = order + orderYear + orderMonth + results;
                    orderMgmtDA.razorpayNewOrder(req, res, orderID);
                }
            }

        })

    } catch (error) {
        console.log(error);
    }
}

exports.updatePOstatus = function (req, res) {
    try {
        orderMgmtDA.updatePOstatus(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.checkCouponOnCustomer = function (req, res) {
    try {
        orderMgmtDA.checkCouponOnCustomer(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.checkCustomerOrderForFirtTimeCoupon = function (req, res) {
    try {
        orderMgmtDA.checkCustomerOrderForFirtTimeCoupon(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getAllOrderWithDate = function(req, res){
    try {
        orderMgmtDA.getAllOrderWithDate(req,res);
    } catch(error) {
        console.log(error);
    }
}

