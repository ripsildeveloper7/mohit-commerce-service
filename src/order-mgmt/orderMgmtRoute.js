var orderMgmtMgr = require('./orderMgmtMgr');

module.exports = function(app) {
    app.route('/getOrders')
.get(orderMgmtMgr.viewOrders);
app.route('/customersorders/:id')
.get(orderMgmtMgr.viewCustomerOrders);
app.route('/singleorders/:id')
.get(orderMgmtMgr.uniqueOrderView);
app.route('/totalorders')
.get(orderMgmtMgr.totalOrdersCount);
app.route('/statusupdate/:id')
.put(orderMgmtMgr.orderStatusUpdate);
app.route('/order')
.put(orderMgmtMgr.newOrderAdd);

app.route('/addrazorpay/:id/total/:total')
.put(orderMgmtMgr.addRazorPayDetails);

// testing
/* app.route('/shiprocketorder')
.post(orderMgmtMgr.newOrder); */


// paypal test
app.route('/paypalorder')
.get(orderMgmtMgr.paypalOrder);

// order through razorpay
app.route('/razorpayorder')
.put(orderMgmtMgr.razorpayNewOrder);

app.route('/updatepurchaseorderstatus/:id')
.put(orderMgmtMgr.updatePOstatus);

app.route('/checkcouponcodebycustomer')
.post(orderMgmtMgr.checkCouponOnCustomer);

app.route('/checkcouponforfirstcustomer/:id')
.get(orderMgmtMgr.checkCustomerOrderForFirtTimeCoupon);

app.route('/getallorderwithdate')
.get(orderMgmtMgr.getAllOrderWithDate);
} 

 