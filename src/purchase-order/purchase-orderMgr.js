var poSettingDA = require('./purchase-orderDA');
var purchaseOrder = require('../model/purchaseOrder.model');

var fonts = {
    Roboto: {
      normal: 'fonts/Roboto-Regular.ttf',
      bold: 'fonts/Roboto-Medium.ttf',
      italics: 'fonts/Roboto-Italic.ttf',
      bolditalics: 'fonts/Roboto-MediumItalic.ttf'
    }
  };
  var nodemailer = require('nodemailer');
  var pdfMake = require('pdfmake');
  var printer = new pdfMake(fonts);
  var fs = require('fs');

exports.getPOsetting = function (req, res) {
    try {
        poSettingDA.getPOsetting(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.CGSTsetting = function (req, res) {
    try {
        poSettingDA.CGSTsetting(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.SGSTsetting = function (req, res) {
    try {
        poSettingDA.SGSTsetting(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.IGSTsetting = function (req, res) {
    try {
        poSettingDA.IGSTsetting(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.ContactNosetting = function (req, res) {
    try {
        poSettingDA.ContactNosetting(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.TermsAndConditionsetting = function (req, res) {
    try {
        poSettingDA.TermsAndConditionsetting(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.POTypeSetting = function (req, res) {
    try {
        poSettingDA.POTypeSetting(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.GSTINsetting = function (req, res) {
    try {
        poSettingDA.GSTINsetting(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deletePOType = function (req, res) {
    try {
        poSettingDA.deletePOType(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.billingAddress = function (req, res) {
    try {
        poSettingDA.billingAddress(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.sendMailWithPdf = function (req, res) {
    try {
        /* pdfMake.vfs = pdfFonts.pdfMake.vfs; */
        console.log(req.body);
        let transporter = nodemailer.createTransport({
            service: 'Godaddy',
            host: "smtpout.secureserver.net",
            secureConnection: true,
            port: 465,
            auth: {
                user: 'support@ucchalfashion.com',
                pass: 'Enterprise@2019'
            },
            debug: true,
        });
        var mailOptions = {
            from: 'support@ucchalfashion.com',
            to: 'ripsildeveloper4@gmail.com',
            subject: 'PO',
            attachments: [{
                filename: `file.pdf`,
                /* content: new Buffer(req.body.document, 'base64'), */
                content: req.body.document,
                encoding: 'base64',
                /* path: 'data:application/pdf;base64,' + req.body.document.toString('base64') */
                contentType: 'application/pdf'
              }],
            /* html: '<html><body> <p> Congratulations, <br>  Your promo code is : UCLP1001. Kindly use the promo code on the day of launch to get exciting discounts as launch offer. We are eagerly looking forward for your visit to our website www.ucchalfashion.com.</p></body></html>' */
        };
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log(info.response);

            }
        });
       /*  var array = req.body; */

        /* array.getBase64(element => {
            console.log(element);
        }) */
        /* console.log(Buffer.from(req.body, 'base64')); */
        /* console.log(req.body);
        function base64Encode(file) {
            var body = fs.readFileSync(file);
            return body.toString('base64');
        }
        var base64String = base64Encode(req.body);
console.log(base64String); */
       /*  var pdfDoc = printer.createPdfKitDocument(req.body);
pdfDoc.pipe(fs.createWriteStream('document.pdf'));
pdfDoc.end(); */
/* console.log(pdf); */
    } catch (error) {
        console.log(error);
    }
}

exports.createPurchaseOrder = function(req, res) {
    try {
        purchaseOrder.find({}).select().exec(function(err, findData) {
            if (err) {
                res.status(500).json(err);
            } else {
                if (findData.length === 0) {
                    const num = 0;
                    poSettingDA.createPurchaseOrder(req, res, num);
                } else {
                    var poLength = findData.length - 1;
                    var splitData = findData[poLength].poNumber.split('/');
                    var num = parseInt(splitData[2]);
                    poSettingDA.createPurchaseOrder(req, res, num);
                }
            }
        })
    } catch (error) {
        console.log(error);
    }
}
exports.getAllPurchaseOrder = function (req, res) {
    try {
        poSettingDA.getAllPurchaseOrder(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getAllPurchaseOrderCount = function (req, res) {
    try {
        poSettingDA.getAllPurchaseOrderCount(req, res);
    } catch (error) {
        console.log(error);
    }
}