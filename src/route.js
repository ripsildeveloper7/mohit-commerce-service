var orderMgmtRoute = require('./order-mgmt/orderMgmtRoute');
var shipmentRoute = require('./shipment/shipmentRoute');
var purchaseOrderRoute = require('./purchase-order/purchase-orderRoute');
var shipRocketRoute = require('./shipRocket/shipRocketRoute');
exports.loadRoutes = function (app) {
    orderMgmtRoute(app);
    shipmentRoute(app);
    purchaseOrderRoute(app);
    shipRocketRoute(app);
};
