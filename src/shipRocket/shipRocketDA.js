var http = require('http');
var ShiprocketOrder = require('../model/shiprockerOrder.model');

exports.createBearerToken = function (req, res) {
  var options = {
    'method': 'POST',
    'hostname': 'https://apiv2.shiprocket.in',
    'path': '/v1/external/auth/login',
    'headers': {
      'Content-Type': 'application/json'
    }
  };

  var req = http.request(options, function (res) {
    var chunks = [];

    res.on("data", function (chunk) {
      chunks.push(chunk);
      console.log('chunks', chunks);
    });

    res.on("end", function (chunk) {
      var body = Buffer.concat(chunks);
      console.log(body.toString());
    });

    res.on("error", function (error) {
      console.error(error);
    });
  });
  console.log(req.body);
  var postData = "{\n    \"email\": \"debesh.km@gmail.com\",\n    \"password\": \"Debesh@123\"\n}";;
  /*  */

  req.write(postData);

  req.end();
}

exports.getChannels = function (req, res) {
  var request = require('request');
  var headers = {
    'Content-Type': 'application/json',
    // future retreive the bearer token from database
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
  };

  var options = {
    url: 'https://apiv2.shiprocket.in/v1/external/channels',
    headers: headers
  };

  function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      var data = JSON.parse(body)
      res.status(200).json(data);
    }
  }
  request(options, callback);
}

exports.createCustomOrder = function (req, res) {
  var request = require('request');

  var headers = {
    'Content-Type': 'application/json',
    // future retreive the bearer token from database
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
  };
  var data = JSON.stringify(req.body); // order details
  /*  var dataString = {
     'order_id' : "224-477",
     "order_date": "2019-07-24 11:11",
     "pickup_location": "Jammu",
     "channel_id": "12345",
     "comment": "Reseller: M/s Goku",
     "billing_customer_name": "Naruto",
     "billing_last_name": "Uzumaki",
     "billing_address": "House 221B, Leaf Village",
     "billing_address_2": "Near Hokage House",
     "billing_city": "New Delhi",
     "billing_pincode": "110002",
     "billing_state": "Delhi",
     "billing_country": "India",
     "billing_email": "naruto@uzumaki.com",
     "billing_phone": "9876543210",
     "shipping_is_billing": true,
     "shipping_customer_name": "",
     "shipping_last_name": "",
     "shipping_address": "",
     "shipping_address_2": "",
     "shipping_city": "",
     "shipping_pincode": "",
     "shipping_country": "",
     "shipping_state": "",
     "shipping_email": "",
     "shipping_phone": "",
     "order_items": [
       {
         "name": "Kunai",
         "sku": "chakra123",
         "units": 10,
         "selling_price": "900",
         "discount": "",
         "tax": "",
         "hsn": 441122
       }
     ],
     "payment_method": "Prepaid",
     "shipping_charges": 0,
     "giftwrap_charges": 0,
     "transaction_charges": 0,
     "total_discount": 0,
     "sub_total": 9000,
     "length": 10,
     "breadth": 15,
     "height": 20,
     "weight": 2.5
   };
    */
  var options = {
    url: 'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc',
    method: 'POST',
    headers: headers,
    body: data
  };

  function callback(error, response, body) {

    console.log(body, 'error');
    if (!error && response.statusCode == 200) {
      console.log(body);
      res.status(200).json(body);

    } else {
      res.status(500).json(body);
    }
  }

  request(options, callback);

}


exports.getPickupAddress = function (req, res) {
  var request = require('request');

  var headers = {
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
  };

  var options = {
    url: 'https://apiv2.shiprocket.in/v1/external/settings/company/pickup',
    headers: headers
  };

  function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      var data = JSON.parse(body);
      res.status(200).json(data);
    }
  }

  request(options, callback);
}

exports.getSingleShiprocket = function (req, res) {
  var request = require('request');
  var orderId = req.params.id;

  var headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
  };

  var options = {
    url: 'https://apiv2.shiprocket.in/v1/external/orders/show/' + orderId,
    headers: headers
  };

  function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      var data = JSON.parse(body);
      res.status(200).json(data);
    }
  }

  request(options, callback);
}
exports.getShiprocketOrder = function (req, res) {
  var request = require('request');

  var headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
  };

  var options = {
    url: 'https://apiv2.shiprocket.in/v1/external/orders',
    headers: headers
  };

  function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      var data = JSON.parse(body);
      res.status(200).json(data);
    }
  }

  request(options, callback);

}

exports.createInvoice = function (req, res) {
  var request = require('request');

  var headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
  };


  var dataString = {
    "ids": [req.params.id]

  };
  id = JSON.stringify(dataString);
  var options = {
    url: 'https://apiv2.shiprocket.in/v1/external/orders/print/invoice',
    method: 'POST',
    headers: headers,
    body: id
  };

  function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      var data = JSON.parse(body);
      res.status(200).json(data);
    }
  }

  request(options, callback);

}

exports.checkCourierAvailability = function (req, res) {
  var request = require('request');

  var pickup_postcode = req.body.pickup_postcode;
  var delivery_postcode = req.body.delivery_postcode;
  var order_id = req.body.order_id;
  var cod = req.body.cod;
  var headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
  };

  var options = {
    url: 'https://apiv2.shiprocket.in/v1/external/courier/serviceability?pickup_postcode=' + pickup_postcode + '&delivery_postcode=' + delivery_postcode + '&order_id=' + order_id + '&cod=' + cod,
    headers: headers
  };

  function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      var data = JSON.parse(body);
      res.status(200).json(data);
    }
  }

  request(options, callback);
}

exports.shiprocketOrder = function (req, res) {
  var shiprocketorder = new ShiprocketOrder();
  shiprocketorder.order_id = req.body.order_id;
  shiprocketorder.shipment_id = req.body.shipment_id;
  shiprocketorder.status = req.body.status;
  shiprocketorder.websiteOrderId = req.body.websiteOrderId;
  shiprocketorder.onboarding_completed_now = req.body.onboarding_completed_now;
  shiprocketorder.orderObjectId = req.body.orderObjectId;
  shiprocketorder.save(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}

exports.cancelOrder = function (req, res) {
  var request = require('request');

  var headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
  };

  var dataString = {
    "ids": [req.params.id]
  };
  id = JSON.stringify(dataString);
  var options = {
    url: 'https://apiv2.shiprocket.in/v1/external/orders/cancel',
    method: 'POST',
    headers: headers,
    body: id
  };

  function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      var data = JSON.parse(body);
      res.status(200).json(data);
    }
  }

  request(options, callback);

}

exports.downloadInvoice = function (req, res) {
  var request = require('request');

  var headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
  };

  var dataString =
  {
    "ids": [req.params.id]

  };
  var id = JSON.stringify(dataString);
  var options = {
    url: 'https://apiv2.shiprocket.in/v1/external/orders/print/invoice',
    method: 'POST',
    headers: headers,
    body: id
  };

  function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      var data = JSON.parse(body);
      res.status(200).json(data);
    }
  }

  request(options, callback);

}

exports.generateAWB = function (req, res) {
  var request = require('request');

  var headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
  };

  var dataString = {
    "shipment_id": req.params.shipmentid,
    "courier_id": req.params.courierid
  };
  ids = JSON.stringify(dataString);
  var options = {
    url: 'https://apiv2.shiprocket.in/v1/external/courier/assign/awb',
    method: 'POST',
    headers: headers,
    body: ids
  };

  function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      var data = JSON.parse(body);
      res.status(200).json(data);
    }
  }

  request(options, callback);

}

exports.printLabel = function (req, res) {
  var request = require('request');

var headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
};

var dataString = {
	"shipment_id": [req.params.shipmentid]
};
ids = JSON.stringify(dataString);
var options = {
    url: 'https://apiv2.shiprocket.in/v1/external/courier/generate/label',
    method: 'POST',
    headers: headers,
    body: ids
};

function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
        console.log(body);
        var data = JSON.parse(body);
        res.status(200).json(data);
    }
}

request(options, callback);

}

exports.readyToPickup = function(req, res) {
  var request = require('request');

  var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
  };
  
  var dataString = {
    "shipment_id": [req.params.shipmentid]
    
  };
  ids = JSON.stringify(dataString);
  var options = {
      url: 'https://apiv2.shiprocket.in/v1/external/courier/generate/pickup',
      method: 'POST',
      headers: headers,
      body: ids
  };
  
  function callback(error, response, body) {
      if (!error && response.statusCode == 200) {
        var data = JSON.parse(body);
        res.status(200).json(data);
      }
  }
  
  request(options, callback);
}

exports.printManifest = function(req, res) {
  var request = require('request');

  var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
  };
  
  var options = {
      url: 'https://apiv2.shiprocket.in/v1/external/manifests/print',
      headers: headers
  };
  
  function callback(error, response, body) {
      if (!error && response.statusCode == 200) {
        var data = JSON.parse(body);
        res.status(200).json(data);
      }
  }
  
  request(options, callback);
  
}
exports.trackThroughAwb = function(req, res) {
  var request = require('request');

  var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
  };
  awbNo = req.params.awbno;
  var options = {
      url: 'https://apiv2.shiprocket.in/v1/external/courier/track/awb/'+awbNo,
      headers: headers
  };
  
  function callback(error, response, body) {
      if (!error && response.statusCode == 200) {
        var data = JSON.parse(body);
        res.status(200).json(data);
      }
  }
  
  request(options, callback);
}

exports.trackShipmentId = function(req, res) {
  var request = require('request');

  var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
  };
  id = req.params.shipmentid;
  var options = {
      url: 'https://apiv2.shiprocket.in/v1/external/courier/track/shipment/'+id,
      headers: headers
  };
  
  function callback(error, response, body) {
      if (!error && response.statusCode == 200) {
        var data = JSON.parse(body);
        res.status(200).json(data);
      }
  }
  
  request(options, callback);
}

exports.trackOrderId = function(req, res) {
  var request = require('request');
  var headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyODE5NywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzU5Nzk1MDMsImV4cCI6MTU3Njg0MzUwMywibmJmIjoxNTc1OTc5NTAzLCJqdGkiOiJaTGFCT01lUE1JZ05LS3pjIn0.04TcxfqqlFJQUAQDtoeVGYnS2_XXret7CbA74VLkdLY'
};
  var options = {
      url: 'https://apiv2.shiprocket.in/v1/external/courier/track?order_id=ORDER25&channel_id=388871',
      headers: headers
  };
  
  function callback(error, response, body) {
      if (!error && response.statusCode == 200) {
          console.log(body);
      }
  }
  
  request(options, callback);
  
}

exports.addAWBToOrder = function(req, res) {
  ShiprocketOrder.findOne({'order_id': req.params.orderid}).select().exec(function(err, order) {
    if (err) {
      res.status(500).json(err);
    } else {
      order.awbNo = req.body.awb;
      order.save(function(err, update) {
        if (err) {
          res.status(500).json(err);
        } else {
          res.status(200).json(update);
        }
      })
    }
  })
}