var shipRocketDA = require('./shipRocketDA');



exports.createBearerToken = function (req, res) {
    try {
        shipRocketDA.createBearerToken(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.getChannels = function(req, res) {
    try {
        shipRocketDA.getChannels(req, res);
    } catch {

    }
}

exports.createCustomOrder = function(req, res) {
    try {
        shipRocketDA.createCustomOrder(req, res);
    } catch {

    }
}

exports.getPickupAddress = function(req, res) {
    try {
        shipRocketDA.getPickupAddress(req, res);
    } catch {

    }
}

exports.getShiprocketOrder = function(req, res) {
    try {
        shipRocketDA.getShiprocketOrder(req, res);
    } catch {

    }
}

exports.getSingleShiprocket = function(req, res) {
    try {
        shipRocketDA.getSingleShiprocket(req, res);
    } catch {

    }
}



exports.createInvoice = function(req, res) {
    try {
        shipRocketDA.createInvoice(req, res);
    } catch {

    }
}


exports.checkCourierAvailability = function(req, res) {
    try {
        shipRocketDA.checkCourierAvailability(req, res);
    } catch {

    }
}


exports.shiprocketOrder = function(req, res) {
    try {
        shipRocketDA.shiprocketOrder(req, res);
    } catch {

    }
}

exports.cancelOrder = function(req, res) {
    try {
        shipRocketDA.cancelOrder(req, res);
    } catch {

    }
}

exports.downloadInvoice = function(req, res) {
    try {
        shipRocketDA.downloadInvoice(req, res);
    } catch {

    }
}

exports.generateAWB = function(req, res) {
    try {
        shipRocketDA.generateAWB(req, res);
    } catch {

    }
}

exports.printLabel = function(req, res) {
    try {
        shipRocketDA.printLabel(req, res);
    } catch {

    }
}

exports.readyToPickup = function(req, res) {
    try {
        shipRocketDA.readyToPickup(req, res);
    } catch {

    }
}
exports.printManifest = function(req, res) {
    try {
        shipRocketDA.printManifest(req, res);
    } catch {

    }
}

exports.trackThroughAwb = function(req, res) {
    try {
        shipRocketDA.trackThroughAwb(req, res);
    } catch {

    }
}
exports.trackShipmentId = function(req, res) {
    try {
        shipRocketDA.trackShipmentId(req, res);
    } catch {

    }
}
exports.trackOrderId = function(req, res) {
    try {
        shipRocketDA.trackOrderId(req, res);
    } catch {

    }
}
exports.addAWBToOrder = function(req, res) {
    try {
        shipRocketDA.addAWBToOrder(req, res);
    } catch {

    }
}

