var shipRocketSettingsMgr = require('./shipRocketMgr');

module.exports = function(app) {

    // create Bearer token
    app.route('/bearertoken')
.post(shipRocketSettingsMgr.createBearerToken);

// get channel deta
app.route('/getchannels')
.get(shipRocketSettingsMgr.getChannels);

// create custom order
app.route('/customorder')
.post(shipRocketSettingsMgr.createCustomOrder);

// get all pick up address
app.route('/getpickupaddress')
.get(shipRocketSettingsMgr.getPickupAddress);

// get all shiprocket orders 
app.route('/getshiprocketorder')
.get(shipRocketSettingsMgr.getShiprocketOrder);

// get single shiprocket orders 
app.route('/singleorder/:id')
.get(shipRocketSettingsMgr.getSingleShiprocket);

// create Invoice
app.route('/createinvoice/:id')
.get(shipRocketSettingsMgr.createInvoice);


// check courier service availability

app.route('/courier')
.post(shipRocketSettingsMgr.checkCourierAvailability);


// create a shiprocket order
app.route('/shiprocketorder')
.post(shipRocketSettingsMgr.shiprocketOrder)

// cancel an order
app.route('/externalorder/cancel/:id')
.get(shipRocketSettingsMgr.cancelOrder);

// download invoice 
app.route('/downloadinvoice/:id')
.get(shipRocketSettingsMgr.downloadInvoice);

// generate awb
app.route('/generate/:shipmentid/awb/:courierid')
.get(shipRocketSettingsMgr.generateAWB);

// print label
app.route('/printlabel/:shipmentid')
.get(shipRocketSettingsMgr.printLabel);

// Ready to  Pickup
app.route('/readytopickup/:shipmentid')
.get(shipRocketSettingsMgr.readyToPickup);

// Print Manifest
app.route('/manifests/print/:shipmentid')
.get(shipRocketSettingsMgr.printManifest);

// Track through awb
app.route('/awb/:awbno')
.get(shipRocketSettingsMgr.trackThroughAwb);

// Track through shipment id
app.route('/trackshipment/:shipmentid')
.get(shipRocketSettingsMgr.trackShipmentId);

// track through order id
app.route('/trackorder/:orderid')
.get(shipRocketSettingsMgr.trackOrderId);
// add awb details to the respective order
app.route('/addawb/:orderid/awb/:awbNo')
.get(shipRocketSettingsMgr.addAWBToOrder);
}