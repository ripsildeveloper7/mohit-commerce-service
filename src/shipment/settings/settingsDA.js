
var ShipmentArea = require('../../model/shippingArea.model');

exports.createShipmentArea = function (req, res) {
    ShipmentArea.find({}).select().exec(function (err, shipmentData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var shipment = new ShipmentArea(req.body);
            if (shipmentData.length === 0) {
                shipment.save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while saving data'
                        })
                    } else {
                        ShipmentArea.find({}).select().exec(function (err, shipmentData) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(shipmentData);
                            }
                        })
                    }
                })
            } else {
                shipmentData[0].serviceType = req.body.serviceType;
                shipmentData[0].pincodes = req.body.pincodes;
                shipmentData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while saving data'
                        })
                    } else {
                        ShipmentArea.find({}).select().exec(function (err, shipmentData) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(shipmentData);
                            }
                        })
                    }
                })
            }
        }
    });
}
exports.getShipmentArea = function (req, res) {
    ShipmentArea.find({}).select().exec(function (err, shipmentData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            res.status(200).json(shipmentData);
        }
    })
}