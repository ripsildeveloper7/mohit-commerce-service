var settingsDA = require('./settingsDA');

exports.createShipmentArea = function (req, res) {
    try {
        settingsDA.createShipmentArea(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getShipmentArea = function (req, res) {
    try {
        settingsDA.getShipmentArea(req, res);
    } catch (error) {
        console.log(error);
    }
}
