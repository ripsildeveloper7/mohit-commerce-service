var settingsMgr = require('./settings/settingsMgr');

module.exports = function(app) {
    app.route('/shipmentarea')
.post(settingsMgr.createShipmentArea);
app.route('/shipmentarea')
.get(settingsMgr.getShipmentArea);
}